const generateSelectOptions = rows => {
  let options = '';

  rows.forEach(item => {
    options += `<option>${item}</option>`
  })

  return options;
};

export const generateSelect = (options, attr) => {
  return `<div style="margin-top:10px">
    <select id="popup-marker-window" data-id="${attr}">
      ${generateSelectOptions(options)}
    </select>
    <button id="popup-marker-delete" style="color:red" data-id="${attr}">X</button>
  </div>`;
}

export const markerMixins = {
  data() {
    return {
      markers: [
        {value: 0, color: 'black', label: 'Zero'},
        {value: 1, color: 'gray', label: 'One'},
        {value: 2, color: 'red', label: 'Two'},
        {value: 3, color: 'orange', label: 'Three'},
        {value: 4, color: 'lime', label: 'Four'},
        {value: 5, color: 'green', label: 'Five'}
      ]
    }
  }
}
