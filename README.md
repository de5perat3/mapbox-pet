# Mapbox - Vue.js

## Demo - https://de5perat3.gitlab.io/mapbox-pet


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

```
